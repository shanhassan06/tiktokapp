import { NavigationContainer } from '@react-navigation/native';
import { persistor, store } from '@redux/store';
import { navigationRef } from '@services/nav.service';
import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Routes from './src/routes';
import { initialConfig } from '@utils/config';
import SplashScreen from 'react-native-splash-screen'
import { StatusBar } from 'react-native';

const App = () => {

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 2000)
    initialConfig()
  }, [])

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <SafeAreaProvider>
          <StatusBar backgroundColor="rgba(0, 0, 0, 0.2)"
            barStyle="dark-content"
            translucent={true} />
          <NavigationContainer ref={navigationRef}>
            <Routes />
          </NavigationContainer>
        </SafeAreaProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;

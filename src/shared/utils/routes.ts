export const ROUTES = {
  LOGIN: 'login',
  HOME: 'home',
  FRIENDS: 'friends',
  INBOX: 'inbox',
  UPLOAD: 'upload',
  PROFILE: 'profile'

};

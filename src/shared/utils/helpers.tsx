import React from 'react';
import { Alert } from 'react-native'


export const displayAlert = (
  title: string,
  message: string,
  isCancellable: Boolean,
  okAction?: any,
  okayText: string = 'OK',
  cancelText: string = 'Cancel',
) => {
  Alert.alert(
    title,
    message,
    isCancellable
      ? [
        {
          text: cancelText,
          onPress: () => {
            okAction(false);
          },
          style: 'cancel',
        },
        {
          text: okayText,
          onPress: () => {
            okAction(true);
          },
        },
      ]
      : [
        {
          text: 'OK',
          onPress: () => {
            okAction(true);
          },
        },
      ],
  );
};

export const getInitials = (user: { firstName: string, lastName: string }) => {
  const initials = user?.firstName.charAt(0) + user?.lastName.charAt(0);
  return initials?.toUpperCase();
}

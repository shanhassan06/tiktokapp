import { createSlice } from '@reduxjs/toolkit';

interface State {
  generalData: any;
}

const initialState: State = {
  generalData: null,
};

export const generalDataReducer = createSlice({
  name: 'generalData',
  initialState,
  reducers: {
    setGeneralData: (state, action) => {
      state.generalData = action.payload;
    },
  },
});

export const { setGeneralData } = generalDataReducer.actions;

export default generalDataReducer.reducer;

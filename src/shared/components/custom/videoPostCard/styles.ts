import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  loaderContainer: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },
  video: {
    height: '100%', width: '100%'
  },
});

export default styles;

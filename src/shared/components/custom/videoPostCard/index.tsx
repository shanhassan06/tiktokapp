import { COLORS } from '@shared/theme/colors';
import React, { useEffect, useState } from 'react';
import {
  View,
  TouchableWithoutFeedback,
  ActivityIndicator,
} from 'react-native';

import Video from 'react-native-video';
import styles from './styles';
interface PostProps {
  post: string,
  viewableIndex: number,
  index: number,
  isFocused: boolean,
  height: number
}


const VideoPostCard = (props: PostProps) => {
  const [paused, setPaused] = useState(true);
  const [loading, setLoading] = useState(false);
  const { post, viewableIndex, index, isFocused, height } = props
  const togglePlay = () => {
    setPaused(!paused);
  };

  useEffect(() => {
    if (viewableIndex == index && isFocused) {
      setPaused(false);
    } else {
      setPaused(true);
    }

  }, [viewableIndex, isFocused]);
  return (
    <View
      style={{
        width: '100%',
        height: height,
      }}>
      <TouchableWithoutFeedback onPress={togglePlay}>
        <View>
          <Video
            onLoadStart={() => setLoading(true)}
            onLoad={() => setLoading(false)}
            source={{ uri: post }}
            style={styles.video}
            onError={(e) => console.log(e)}
            resizeMode={'cover'}
            repeat={true}
            paused={paused}
          />
        </View>
      </TouchableWithoutFeedback>
      {loading && (
        <View style={styles.loaderContainer}>
          <ActivityIndicator size="large" color={COLORS.white} />
        </View>
      )}
    </View>
  );
};

export default VideoPostCard;

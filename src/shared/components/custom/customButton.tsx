
import { COLORS } from '@theme/colors';
import { moderateScale } from '@theme/metrics';
import React from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';

interface header {
    text?: string,
    onPress?: any,
    textStyle?: any,
    wrapStyle?: any,
    displayCar?: any
}

const CustomButton = ({
    onPress,
    text,
    textStyle,
    wrapStyle,
    displayCar
}: header) => {
    return (
        <TouchableOpacity
            onPress={onPress}
            style={[styles.login, wrapStyle]}>
            <Text style={[styles.text, textStyle]}>
                {text}
            </Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    text: { fontWeight: 'bold', color: COLORS.white, fontSize: moderateScale(20) },
    login: {
        width: "80%",
        alignSelf: 'center',
        padding: moderateScale(15),
        borderRadius: 10,
        borderWidth: moderateScale(1),
        borderColor: COLORS.white,
        flexDirection: 'row',
        justifyContent: 'center'
    },
});

export default CustomButton;

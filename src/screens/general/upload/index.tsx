import React, { } from 'react';
import { View, Text } from 'react-native';
import Wrapper from '@shared/components/wrapper';
import styles from './styles';

const Upload = () => {
  return (
    <Wrapper top>
      <View style={styles.container}>
        <Text style={styles.textStyle}>Upload Screen</Text>
      </View>
    </Wrapper>
  );
};

export default Upload;

import React, { } from 'react';
import { View, Text } from 'react-native';
import Wrapper from '@shared/components/wrapper';
import styles from './styles';

const Inbox = () => {
  return (
    <Wrapper top>
      <View style={styles.container}>
        <Text style={styles.textStyle}>Inbox Screen</Text>
      </View>
    </Wrapper>
  );
};

export default Inbox;

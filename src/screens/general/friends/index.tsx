import React from 'react';
import { View, Text } from 'react-native';
import Wrapper from '@shared/components/wrapper';
import styles from './styles';

const Friends = () => {
  return (
    <Wrapper top>
      <View style={styles.container}>
        <Text style={styles.textStyle}>Friends Screen</Text>
      </View>
    </Wrapper>
  );
};

export default Friends;

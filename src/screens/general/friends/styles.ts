import { COLORS } from "@shared/theme/colors";
import { moderateScale } from "@shared/theme/metrics";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: COLORS.black
    },
    textStyle: {
        color: COLORS.white,
        fontSize: moderateScale(20)
    }
})

export default styles
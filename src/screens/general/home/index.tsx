import React, { useState, useRef, useEffect } from 'react';
import { FlatList, Dimensions } from 'react-native';
import Wrapper from '@shared/components/wrapper';
import { moderateScale } from '@shared/theme/metrics';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { COLORS } from '@shared/theme/colors';
import { useIsFocused } from '@react-navigation/native';
import { VIDEOS_LINKS } from '@shared/utils/constants';
import VideoPostCard from '@shared/components/custom/videoPostCard';

const Home = () => {

  const [activeIndex, setActiveIndex] = useState(0);

  const onViewableItemsChanged = ({ viewableItems }: any) => {
    setActiveIndex(viewableItems[0].index);
  };
  const viewabilityConfigCallbackPairs = useRef<any>([{ onViewableItemsChanged }]);
  const insets = useSafeAreaInsets()
  const isFocused = useIsFocused()
  let height = Dimensions.get('window').height - moderateScale(50) - insets.top
  return (
    <Wrapper top>
      <FlatList
        data={VIDEOS_LINKS}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => (
          <VideoPostCard
            height={height}
            isFocused={isFocused}
            index={index}
            viewableIndex={activeIndex}
            post={item}
          />
        )}
        contentContainerStyle={{ backgroundColor: COLORS.black }}
        showsVerticalScrollIndicator={false}
        snapToInterval={height}
        snapToAlignment={'start'}
        decelerationRate={'fast'}
        viewabilityConfigCallbackPairs={viewabilityConfigCallbackPairs.current}
        viewabilityConfig={{
          itemVisiblePercentThreshold: height,
        }}
        initialNumToRender={10}
        maxToRenderPerBatch={5}
        removeClippedSubviews={true}
      />
    </Wrapper>
  );
};

export default Home;

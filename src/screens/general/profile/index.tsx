import React, { } from 'react';
import { View, Text } from 'react-native';
import Wrapper from '@shared/components/wrapper';
import styles from './styles';
import { moderateScale } from '@shared/theme/metrics';
import CustomButton from '@shared/components/custom/customButton';
import { setUser } from '@shared/redux/reducers/userReducer';
import { useDispatch } from 'react-redux';

const Profile = () => {
  const dispatch = useDispatch()
  const handleSubmit = async () => {
    dispatch(setUser(null));
  };
  return (
    <Wrapper top>
      <View style={styles.container}>
        <Text style={styles.textStyle}>Profile Screen</Text>
        <CustomButton
          text="Log Out"
          onPress={() => handleSubmit()}
          wrapStyle={{ marginTop: moderateScale(50) }}
        />
      </View>
    </Wrapper>
  );
};

export default Profile;

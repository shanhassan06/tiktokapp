import { StyleSheet } from 'react-native';
import { moderateScale } from '@theme/metrics';
import { COLORS } from '@theme/colors';
import { FONTS } from '@theme/fonts';

const styles = StyleSheet.create({
  wrapperStyle: {
    width: '100%'
  },
  container: {
    backgroundColor: COLORS.black,
    flex: 1,
    width: '100%',
    padding: moderateScale(20)
  },
  heading: {
    color: COLORS.white,
    textAlign: 'center',
    fontSize: moderateScale(35)
  },
  titleText: {
    color: COLORS.white,
    textAlign: 'center',
    fontSize: moderateScale(20),
    paddingTop: moderateScale(10)
  },
  fieldsWrapper: {
    flex: 1,
    justifyContent: 'center'
  },
  inputStyle: {
    color: COLORS.white,
    fontSize: moderateScale(16),
    fontWeight: '500',
    lineHeight: moderateScale(20),
    paddingVertical: moderateScale(6),
    borderBottomWidth: 1,
    marginTop: moderateScale(30)
  },
});

export default styles;

import { COLORS } from '@theme/colors';
import { LoginSchema } from '@utils/validations';
import { Formik } from 'formik';
import React, { } from 'react';
import {
  View,
  Text,
  TextInput,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { setUser } from '@shared/redux/reducers/userReducer';
import { moderateScale } from '@shared/theme/metrics';
import styles from './styles';
import CustomButton from '@shared/components/custom/customButton';
import Wrapper from '@shared/components/wrapper';

const Login = ({ navigation }: any) => {
  const loginValues = { email: '', password: '' }
  const dispatch = useDispatch()

  const LoginF = async (values: any) => {
    dispatch(setUser(values));
  };
  return (
    <Wrapper top style={styles.wrapperStyle}>
      <Formik
        initialValues={loginValues}
        onSubmit={(val: any, { resetForm }: any) => {
          LoginF(val)
          resetForm()
        }}
        validationSchema={LoginSchema}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          setFieldValue,
          values,
          touched,
          errors,
          isValid,
        }) => (

          <View style={styles.container}>
            <Text style={styles.heading}>Assestment</Text>
            <Text style={styles.titleText}>Shan Ul Hassan</Text>
            <View style={styles.fieldsWrapper}>
              <TextInput
                placeholder="Enter email"
                onChangeText={handleChange('email')}
                value={values.email}
                placeholderTextColor={COLORS.lightGrey}
                style={[styles.inputStyle, { borderBottomColor: errors.email && touched.email ? COLORS.red : COLORS.white }]} />
              {errors.email && touched.email && <Text style={{ color: COLORS.red }}>{errors.email}</Text>}
              <TextInput
                placeholder="Password"
                secureTextEntry={true}
                onChangeText={handleChange('password')}
                value={values.password}
                placeholderTextColor={COLORS.lightGrey}
                style={[styles.inputStyle, { borderBottomColor: errors.email && touched.email ? COLORS.red : COLORS.white }]} />
              {errors.password && touched.password && <Text style={{ color: COLORS.red }}>{errors.password}</Text>}
              <CustomButton
                text="Login"
                onPress={handleSubmit}
                wrapStyle={{ marginTop: moderateScale(50) }}
              />
            </View>
          </View>
        )}
      </Formik>
    </Wrapper>
  );
};

export default Login;





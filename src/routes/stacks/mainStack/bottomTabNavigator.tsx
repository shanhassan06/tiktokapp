import React from 'react';
import { Image } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '@screens/general/home';

import Fontisto from 'react-native-vector-icons/Fontisto';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { ROUTES } from '@shared/utils/routes';
import Upload from '@screens/general/upload';
import Inbox from '@screens/general/inbox';
import Profile from '@screens/general/profile';
import { moderateScale } from '@shared/theme/metrics';
import Friends from '@screens/general/friends';
import { COLORS } from '@shared/theme/colors';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          backgroundColor: COLORS.black,
          height: moderateScale(50)
        },
        tabBarActiveTintColor: COLORS.white,
      }}
    >
      <Tab.Screen
        name={ROUTES.HOME}
        component={Home}
        options={{
          tabBarIcon: ({ color }: any) => (
            <Fontisto name={'home'} size={moderateScale(24)} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={ROUTES.FRIENDS}
        component={Friends}
        options={{
          tabBarIcon: ({ color }: any) => (
            <FontAwesome5 name={'user-friends'} size={moderateScale(20)} color={color} />
          ),
        }}
      />
      <Tab.Screen
        name={ROUTES.UPLOAD}
        component={Upload}
        options={{
          tabBarIcon: ({ }) => (
            <Image
              source={require('@assets/images/addIcon.png')}
              style={{ height: 34, resizeMode: 'contain' }}
            />
          ),
          tabBarLabel: () => null,
        }}
      />
      <Tab.Screen
        name={ROUTES.INBOX}
        component={Inbox}
        options={{
          tabBarIcon: ({ color }: any) => (
            <MaterialCommunityIcons
              name={'message-minus-outline'}
              size={moderateScale(24)}
              color={color}
            />
          ),
        }}
      />
      <Tab.Screen
        name={ROUTES.PROFILE}
        component={Profile}
        options={{
          tabBarIcon: ({ color }: any) => (
            <Ionicons name={'person-outline'} size={moderateScale(24)} color={color} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;

import { createStackNavigator } from '@react-navigation/stack';
import Login from '@screens/auth/login';
import { ROUTES } from '@utils/routes';
import React from 'react';

const Stack = createStackNavigator();

const AuthStack = () => {
  return (
    <>
      <Stack.Navigator screenOptions={{ headerShown: false }} >
        <Stack.Screen name={ROUTES.LOGIN} component={Login} />
      </Stack.Navigator>
    </>
  );
};

export default AuthStack;

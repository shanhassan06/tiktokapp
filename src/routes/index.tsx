import React from 'react';
import { useSelector } from 'react-redux';
import AuthStack from './stacks/authStack';
import { Host } from 'react-native-portalize';
import BottomTabNavigator from './stacks/mainStack/bottomTabNavigator';

const Routes = () => {
  const { user } = useSelector((state: any) => state.root.user);

  return (
    <Host>
      {user ? <BottomTabNavigator /> : <AuthStack />}
    </Host>
  )

};

export default Routes;
